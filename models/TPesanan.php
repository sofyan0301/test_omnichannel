<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_pesanan".
 *
 * @property int $id
 * @property string $no_pesanan
 * @property string $tanggal
 * @property string $nm_supplier
 * @property string $nm_produk
 * @property float $total
 */
class TPesanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_pesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_pesanan', 'tanggal', 'nm_supplier', 'nm_produk', 'total'], 'required'],
            [['tanggal'], 'safe'],
            [['total'], 'number'],
            [['no_pesanan'], 'string', 'max' => 20],
            [['nm_supplier', 'nm_produk'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_pesanan' => 'No Pesanan',
            'tanggal' => 'Tanggal',
            'nm_supplier' => 'Nm Supplier',
            'nm_produk' => 'Nm Produk',
            'total' => 'Total',
        ];
    }
}
