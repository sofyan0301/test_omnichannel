<?php
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\grid\GridView;

use yii\bootstrap5\Modal;
use yii\helpers\Html;

Modal::begin([
    'id' => 'my-modal',
    'size' => 'modal-lg',
]);


Modal::end();

/** @var yii\web\View $this */


$this->registerJs(
    "$(document).ready(function() {
        getDataList();
    });
    $('#show-product').click(function() {
        $('.data-content').remove();
        getDataList();
    });
    $(document).on('click', '.show-detail', function(){ 
        let id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: 'https://dummyjson.com/products/' + id,
            dataType: 'json',
            data: {'data':'check'},
            success: function(data){
                console.log(data)
                $('.modal-body').empty();
                let htmlImages = '';
                for (var image of data.images) {
                    htmlImages += '<img src='+image+' style=\'padding:2px\' width=\'100px\' />'
                }
                let starHtml = '';
                for (let i = 0; i < data.rating; i++) {
                    starHtml += '<span class=\'fa fa-star\'></span>';
                }
                
                let html = '<div class=\'row\'><div class=\'col-sm-6\'><label>title</label><br><img src='+data.thumbnail+' width=\'390px\' /><br><br>'+ htmlImages +'</div>' +
                            '<div class=\'col-sm-6\'><label>Price : $'+ data.price +'</label> <label style=\'float: right;margin-right: 30px;\'>' + starHtml + '</label>' +
                            '<p>category : '+ data.category +' <label style=\'float: right;margin-right: 30px;\'>brand : '+ data.brand +'</label></p>' +
                            '<p>stock : '+ data.stock +'</p>' +
                            '<p>description : </p>' +
                            '<p>'+ data.description +'</p>' +
                            '</div></div>';
                $('.modal-body').append(html);
                $('#my-modal').modal('show');
            }
        });
        
    });
    function getDataList(){
        $.ajax({
            type: 'GET',
            url: 'https://dummyjson.com/products',
            dataType: 'json',
            data: {'data':'check'},
            success: function(data){
                let html = '';
                for (var product of data.products) {
                    html += '<tr class=\'data-content\'><td><img src='+product.thumbnail+' width=\'100px\' /></td><td>'+product.title+'</td><td>'+product.category+'</td><td>'+product.brand+'</td><td>'+product.stock+'</td><td>'+product.price+'</td><td><button class=\'btn btn-sm btn-primary show-detail\' data-id='+product.id+' >View</button></td></tr>'
                }
                $('.table').find('tbody').append(html);
            }
        });
    }"
);
$this->title = 'My Yii Application';
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="site-index">


    <div class="body-content">

        <!-- Data Grid Container -->
        <div class="table-responsive">
        <button id="show-product" class="btn btn-primary">Show Product</button>
            <table class="table">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Stock</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>

    </div>
</div>