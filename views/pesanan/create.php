<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TPesanan $model */

$this->title = 'Create T Pesanan';
$this->params['breadcrumbs'][] = ['label' => 'T Pesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tpesanan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
