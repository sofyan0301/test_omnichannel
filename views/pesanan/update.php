<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TPesanan $model */

$this->title = 'Update T Pesanan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'T Pesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tpesanan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
