<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TPesanan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tpesanan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_pesanan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "tanggal")->widget(
        \yii\widgets\MaskedInput::class, [
            'mask' => "y-1-2 h:s",
            'clientOptions' => [
                'alias' => 'datetime',
                "placeholder" => "yyyy-mm-dd hh:mm",
                "separator" => "-"
            ]
        ]
    ); ?>
    
    <?= $form->field($model, 'nm_supplier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_produk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
